import React ,{Component} from 'react';

const intents=[];
class RestController extends Component{
    constructor(props){
        super(props);
        this.state = {intents:[]};
        this.headers = [
            {key:'intent',label:'Intent'},
            {key:'subintent',label:'SubIntent'},
            {key:'point',label:'Point'},
            {key:'synonym',label:'Synonym'}
        ];
    }

    componentDidMount(){
        fetch('https://inter-test-39c9e.firebaseio.com/inter-test-39c9e')
        .then(response => {
            return response.json();
        }).then(result => {
            this.setState({
                intents:result
            });
        });
    }

    render(){
        return(
            <table>
                <thead>
					<tr>
					{
						this.headers.map(function(h) {
                            console.log(intents+"INNNNNNNNN1");
							return (
								<th key = {h.key}>{h.label}</th>
							)
						})
					}
					</tr>
				</thead>
				<tbody>
					{
						this.state.intents.map(function(item, key) {   
                            console.log(intents+"INNNNNNNNN2");
                                      
						return (
								<tr key = {key}>
								  <td>{item.intent}</td>
								  <td>{item.subintent}</td>
								  <td>{item.point}</td>
								  <td>{item.sysnonym}</td>
								</tr>
							)
						})
					}
				</tbody>
            </table>
        )
    }
}
export default RestController;