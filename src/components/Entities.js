import React ,{Component} from 'react';

import Form from "./Form";
import ReactDOM from 'react-dom';
import RestController from './RestController';

class Entities extends Component{
    constructor(props){
        super(props);

        this.state = {
            word:[]
        };

        this.addWord = this.addWord.bind(this);
        this.deleteWord = this.deleteWord.bind(this);
    }

    addWord(intent, synonym){
        this.setState(prevState => ({
            word: [...prevState.word,{intent, synonym}]
        }));
    }

    componentDidMount(){
        this.getWord();
    }

    getWord(){
        fetch("https:jsonplaceholder.typicode.com/words")
        .then(response => response.json())
        .then(response => this.setState({ people: response }))
        .catch(error => console.log(error));
    }

    deleteWord(synonym) {
        return () => {
          this.setState(prevState => ({
            word: prevState.word.filter(word => word.synonym !== synonym)
          }));
        };
      }

    render(){
        return(
            <div style={{textAlign:'center'}}>
                <Form addWord={this.addWord}/>
                <table>
                    <thead>
                        <tr>
                            <th>VALUE</th>
                            <th style={{padding:50}}>SYNONYMS</th>
                        </tr>
                    </thead>
                    <tbody>
                        {this.state.word.map((word, index) => {
                            return(
                                <tr key={word.synonym}>
                                <td>{word.intent}</td>
                                <td>{word.synonym}</td>
                                    <td>
                                        <button onClick={this.deleteWord(word.synonym)}>
                                            Delete
                                        </button>
                                    </td>
                                </tr>
                            );
                        })}
                    </tbody>
                </table>
            </div>
        );
    }
}
export default Entities