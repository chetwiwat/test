import React, { Component } from "react";

class Form extends Component {
  constructor() {
    super();
    this.formSubmit = this.formSubmit.bind(this);
  }

  formSubmit(event) {
    event.preventDefault();
    const form = event.target;
    const intent = form.elements["intent"].value;
    const synonym = form.elements["synonym"].value;
    this.props.addWord(intent, synonym,);
    form.reset();
  }

  render() {
    return (
      <form onSubmit={this.formSubmit}>
        <input id="intent" type="text" defaultValue="" placeholder="Value" />
        <input id="synonym" type="text" defaultValue="" placeholder="Synonym" />
        <input type="submit" value="submit" />
      </form>
    );
  }
}

export default Form;
