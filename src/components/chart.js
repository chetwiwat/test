import React , {Component} from 'react';

import Chart from 'react-apexcharts';

class chart extends Component {
  constructor(props) {
    super(props);

    this.state = {
      value:'all',
      options: {
        labels: ['reset', 'change', 'unlock', 'Reset', 'Change', 'unlock'],
        responsive: [{
          breakpoint: 480,
          options: {
            chart: {
              width: 200
            },
            legend: {
              position: 'bottom'
            }
          }
        }]
      },
      series: [10, 5, 6, 7, 8, 9],
    }
    this.handleChange = this.handleChange.bind(this);
  }

  handleChange(event){
    if(event.target.value=="all"){
      this.setState({value: event.target.value});
    }
    if(event.target.value=="ad"){
      this.setState({value: event.target.value});
    }
    if(event.target.value=="sap"){
      this.setState({value: event.target.value});
    }
  }

      render() {
        return (
          <div>
            <header><h1>Overview</h1></header>
              <div id="chart">
                <select value={this.state.value} onChange={this.handleChange}>
                  <option value="all">All</option>
                  <option value="ad">AD</option>
                  <option value="sap">SAP</option>
                </select>
                {this.state.value == 'all' && <All/> }
                {this.state.value == 'ad' && <Ad/> }
                {this.state.value == 'sap' && <Sap/> }
              </div>
          </div>
        );
      }
}

class All extends Component{

  constructor(props) {
    super(props);

    this.state = {
      value:'all',
      options: {
        labels: ['reset', 'change', 'unlock', 'Reset', 'Change', 'unlock'],
        responsive: [{
          breakpoint: 480,
          options: {
            chart: {
              width: 200
            },
            legend: {
              position: 'bottom'
            }
          }
        }]
      },
      series: [10, 5, 6, 7, 8, 9],
    }
  }

  render() {
    return (
      <div>
            <Chart options={this.state.options} series={this.state.series} type="pie" width="400" />
      </div>
    );
  }
}

class Ad extends Component{

  constructor(props) {
    super(props);

    this.state = {
      options: {
        labels: ['reset', 'change', 'unlock'],
        responsive: [{
          breakpoint: 480,
          options: {
            chart: {
              width: 200
            },
            legend: {
              position: 'bottom'
            }
          }
        }]
      },
      series: [10, 5, 6,],
    }
  }

  render() {
    return (
      <div>
            <Chart options={this.state.options} series={this.state.series} type="pie" width="400" />
      </div>
    );
  }
}

class Sap extends Component{

  constructor(props) {
    super(props);

    this.state = {
      options: {
        labels: ['Reset', 'Change', 'unlock'],
        responsive: [{
          breakpoint: 480,
          options: {
            chart: {
              width: 200
            },
            legend: {
              position: 'bottom'
            }
          }
        }]
      },
      series: [7, 8, 9],
    }
  }

  render() {
    return (
      <div>
            <Chart options={this.state.options} series={this.state.series} type="pie" width="400" />
      </div>
    );
  }
}

export default chart
