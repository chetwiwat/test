import * as firebase from 'firebase';

import firestore from 'firebase/firestore';

const settings = {timestampsInSnapshots:true};

const firebaseConfig = {
    apiKey: "AIzaSyBZ2Kk9L999sokziU7w3yXM-4JQsws0Qw0",
    authDomain: "inter-test-39c9e.firebaseapp.com",
    databaseURL: "https://inter-test-39c9e.firebaseio.com",
    projectId: "inter-test-39c9e",
    storageBucket: "inter-test-39c9e.appspot.com",
    messagingSenderId: "886545723835",
    appId: "1:886545723835:web:842bed72f63f87eb"
};

    firebase.initializeApp(firebaseConfig);
    firebase.firestore().settings(settings);

export default firebase;