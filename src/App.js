import './App.css';

import {Link, Route, BrowserRouter as Router} from 'react-router-dom'
import React,{Component} from 'react';

import Chart from './components/chart';
import Entities from './components/Entities'
import Sidebar from 'react-sidebar';

const mql = window.matchMedia(`(min-width: 800px)`);
const routes =[
  {
    path:"/",
    exact: true,
    main:() => <h2>Home</h2>
  },
  {
    path:"/data-analytics",
    main:() => <Chart/>
  },
  {
    path:"/entities",
    main:() => <Entities/>
  },
];

class App extends Component {

  constructor(props) {
    super(props);
    this.state = {
      sidebarDocked: mql.matches,
      sidebarOpen: true
    };
 
    this.mediaQueryChanged = this.mediaQueryChanged.bind(this);
    this.onSetSidebarOpen = this.onSetSidebarOpen.bind(this);
  }
 
  componentWillMount() {
    mql.addListener(this.mediaQueryChanged);
  }
 
  componentWillUnmount() {
    this.state.mql.removeListener(this.mediaQueryChanged);
  }
 
  onSetSidebarOpen(open) {
    this.setState({ sidebarOpen: open });
  }
 
  mediaQueryChanged() {
    this.setState({ sidebarDocked: mql.matches, sidebarOpen: false });
  }

  render(){
  return (
    <Router>
    <Sidebar className="Navcontain"
    sidebar={
      <div className="App">
        <header className="head">ADMIN CHATBOT</header>
        <Link to="/data-analytics" className="menu1"><img src={require('./icons/bar-chart.png')} height={35} width={35} />DATA ANALYTICS</Link>
        <li className="menu2"><img src={require('./icons/bullet-list.png')} height={35} width={35}/>INTENTS</li>
        <Link to="/entities" className="menu3"><img src={require('./icons/entity.png')} height={35} width={35}/>ENTITIES</Link>
        <li className="menu4"><img src={require('./icons/chat.png')} height={35} width={35}/>CONVERSATION FLOW</li>
        <li className="menu5"><img src={require('./icons/process.png')} height={35} width={35}/>BUSINESS LOGIC</li>
        <li className="menu6"><img src={require('./icons/diamond.png')} height={35} width={35}/>RULE-BASED</li>
        <li className="menu7"><img src={require('./icons/language.png')} height={35} width={35}/>NLP</li>
      </div>
    }
    open={this.state.sidebarOpen}
    docked={this.state.sidebarDocked}
    onSetOpen={this.onSetSidebarOpen}
    >
      <div className="App">
          <header className="Main">
            {routes.map((route, index)=>(
              <Route
                key={index}
                path={route.path}
                exact={route.exact}
                component={route.main}
              />
            ))}
          </header>
        </div>
    </Sidebar>
    </Router>
    );
  }
}

export default App;
